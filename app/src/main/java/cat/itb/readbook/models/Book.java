package cat.itb.readbook.models;

public class Book {
    private int imagen;
    private String titulo;
    private String autor;

    public Book(int imagen, String titulo, String autor) {
        this.imagen = imagen;
        this.titulo = titulo;
        this.autor = autor;
    }

    public int getImagen() {
        return imagen;
    }

    public void setImagen(int imagen) {
        this.imagen = imagen;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }
}
