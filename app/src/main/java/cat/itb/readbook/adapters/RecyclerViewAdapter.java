package cat.itb.readbook.adapters;

import android.app.Dialog;
import android.content.Context;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cat.itb.readbook.R;
import cat.itb.readbook.models.Book;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    Context mContext;
    List<Book> mData;
    Dialog myDialog;

    public RecyclerViewAdapter(Context mContext, List<Book> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View v;
       v= LayoutInflater.from(mContext).inflate(R.layout.item_books,parent,false);
       final MyViewHolder vHolder = new MyViewHolder(v);

       myDialog = new Dialog(mContext);
       myDialog.setContentView(R.layout.info_fragment);

        vHolder.item_books.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView image_book = myDialog.findViewById(R.id.image_book);
                image_book.setImageResource(mData.get(vHolder.getAdapterPosition()).getImagen());
                TextView textView_titulo = myDialog.findViewById(R.id.textView_titulo);
                textView_titulo.setText(mData.get(vHolder.getAdapterPosition()).getTitulo());
                TextView textView_author = myDialog.findViewById(R.id.textView_autor);
                textView_author.setText(mData.get(vHolder.getAdapterPosition()).getAutor());
                myDialog.show();
            }
        });
       return vHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.titulo_libro.setText(mData.get(position).getTitulo());
        holder.autor_libro.setText(mData.get(position).getAutor());
        holder.imagen_libro.setImageResource(mData.get(position).getImagen());

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener{

        private ConstraintLayout item_books;
        private ImageView imagen_libro;
        private TextView titulo_libro;
        private TextView autor_libro;

        public MyViewHolder(View itemView){
            super(itemView);

            item_books = (ConstraintLayout) itemView.findViewById(R.id.item_books_id);
            imagen_libro = (ImageView) itemView.findViewById(R.id.image_book);
            titulo_libro = (TextView) itemView.findViewById(R.id.textView_titulo);
            autor_libro = (TextView) itemView.findViewById(R.id.textView_autor);
            item_books.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

            menu.add(this.getAdapterPosition(),1,0,"DELETE");
            menu.add(this.getAdapterPosition(),2,0,"UPDATE");
        }
    }

    public void deleteItem(int position){
        mData.remove(position);
        notifyDataSetChanged();
    }
}
