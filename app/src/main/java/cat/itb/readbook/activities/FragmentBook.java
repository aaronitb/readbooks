package cat.itb.readbook.activities;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import cat.itb.readbook.R;
import cat.itb.readbook.adapters.RecyclerViewAdapter;
import cat.itb.readbook.models.Book;

public class FragmentBook extends Fragment{

    View v;
    RecyclerView recyclerView;
    public static List<Book> listaLibros = new ArrayList<>();
    public static RecyclerViewAdapter adapter;

    public FragmentBook() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.book_fragment,container,false);
        recyclerView = v.findViewById(R.id.recyclerview_books);
        adapter = new RecyclerViewAdapter(getContext(),listaLibros);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
        return v;
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case 1:
                adapter.deleteItem(item.getGroupId());
                return true;
            case 2:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragmentolibro, new FragmentUpdate()).commit();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
}
