package cat.itb.readbook.activities;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import cat.itb.readbook.R;

public class FragmentUpdate extends Fragment {
    View v;

    String nombreImagen;
    int imagenID;
    Button addButton;
    EditText title;
    EditText author;
    Spinner spinnerImagen;
    ImageView imagen;
    String[] nombreImagenes = {
            "Magic Lessons", "Day Care", "Next Door"
    };

    public FragmentUpdate() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.update_fragment, container, false);
        return v;
    }

}