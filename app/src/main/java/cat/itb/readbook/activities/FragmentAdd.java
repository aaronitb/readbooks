package cat.itb.readbook.activities;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import cat.itb.readbook.R;
import cat.itb.readbook.models.Book;

public class FragmentAdd extends Fragment implements View.OnClickListener {
    View v;

    String nombreImagen;
    int imagenID;
    Button addButton;
    EditText title;
    EditText author;
    Spinner spinnerImagen;
    ImageView imagen;
    String[] nombreImagenes = {
            "Magic Lessons", "Day Care", "Next Door"
    };

    public FragmentAdd() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.add_fragment,container,false);
        title = v.findViewById(R.id.editTitle);
        author = v.findViewById(R.id.editAuthor);
        imagen = v.findViewById(R.id.imagenSpinner);
        spinnerImagen = v.findViewById(R.id.spinner_imagen);
        addButton = v.findViewById(R.id.addButton);
        addButton.setOnClickListener(this);
        lista();
        return v;
    }

    private void lista(){
        ArrayAdapter arrayAdapter = new ArrayAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, nombreImagenes);
        spinnerImagen.setAdapter(arrayAdapter);
        spinnerImagen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (nombreImagenes[0].equals(spinnerImagen.getItemAtPosition(position).toString())) {
                    imagen.setImageResource(R.drawable.libro1);
                    imagenID = R.drawable.libro1;
                    nombreImagen = spinnerImagen.getItemAtPosition(position).toString();
                } else if (nombreImagenes[1].equals(spinnerImagen.getItemAtPosition(position).toString())) {
                    imagen.setImageResource(R.drawable.libro2);
                    imagenID = R.drawable.libro2;
                    nombreImagen = spinnerImagen.getItemAtPosition(position).toString();
                } else if (nombreImagenes[2].equals(spinnerImagen.getItemAtPosition(position).toString())) {
                    imagen.setImageResource(R.drawable.libro3);
                    imagenID = R.drawable.libro3;
                    nombreImagen = spinnerImagen.getItemAtPosition(position).toString();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    public void onClick(View v) {
        String nombre = title.getText().toString();
        String autor = author.getText().toString();
        if (v.getId()==R.id.addButton){
            if (nombre.trim().equals("") || autor.trim().equals("")) {
                Toast.makeText(getActivity(), "Introduce todos los campos", Toast.LENGTH_SHORT).show();
            }else {
                FragmentBook.listaLibros.add(new Book(imagenID, nombre, autor));
                FragmentBook.adapter.notifyDataSetChanged();
            }
        }
    }
}
